#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <stdio.h>

using namespace cv;
using namespace std;

int main(int argc, char* argv[])
{
    CvCapture* capture = cvCreateCameraCapture( 1 ); // Abre la cámara #1
    if (!capture)
    {
        cerr << "No camera available" << endl; // Si no se pudo abrir la cámara manda error.
        return -1;
    }
    //cvSetCaptureProperty(capture, CV_CAP_PROP_FPS, 6);
    
    IplImage* img;

    cvNamedWindow("Image:",1);  // Crea ventana "Image:"
    
    int seq = 0;
    
    if (argc>1)
    {
        sscanf(argv[1],"%d",&seq);
    }
    cout << "starting at " << seq << endl;
    int key;
    while(img = cvQueryFrame( capture ))
    {
        char filename[80];
        cvShowImage("Image:",img);  // Despliega imagen en ventana "Image:"
        key = cvWaitKey(200);                // Espera a que se presione una tecla
        cout << key << endl;
        if (key == 'q') break;
        if (key == 10)
        {
            sprintf(filename,"image%d.tif",seq++);
            cvSaveImage(filename,img);
            cout << filename << " saved" << endl;
        }
    }

    cvDestroyWindow("Image:");	// Cierra ventana "Image:"

    cvReleaseImage( &img );     // Libera la memoria ocupada por img

    cvReleaseCapture( &capture );
    
    return 0; 
}
